//
//  PortNewStation.swift
//  KYGO Music
//
//  Created by Cody Nelson on 11/6/17.
//  Copyright © 2017 Bonneville. All rights reserved.
//

import UIKit
import radio_ui_ios_framework
import Foundation

//MARK: URLS
let kScheduleUrlRoot:           String = "http://tweak.bonnint.com"
let kContestsUrlRoot:           String = "http://kygo.com"
let kCalendarEventsUrlRoot:     String = "http://kygo.com"
let kLatestItemUrlRoot:         String = "http://kygo.com"
let kRecentlyPlayedUrlRoot:     String = "http://api.bonnint.com"
let kOnDemandShowsUrlRoot:      String = "http://tweak.bonnint.com"
let kShowAudioUrlRoot:          String = "http://tweak.bonnint.com"
let kOnDemandHighlightsUrlRoot: String = "http://tweak.bonnint.com"
let kNewsUrlRoot:               String = "http://1043thefan.com"
let kBlackoutsUrlRoot:          String = ""


let kBlackoutsPath:             String = ""
let kBlackoutArea:              String = "Salt Lake City"

//MARK: Feeds

let kContestsUrlPath:           String = "feed/?post_type=contest"
let kSchedulePath:              String = "denver/kkfn/schedule"
let kShowsPath:                 String = "denver/kkfn/shows"
let kShowAudioPath:             String = "denver/kkfn/audio"
let kHighlightGroupPath:        String = "denver/kkfn/highlights"
let kNewsItemsPath:             String = "feed/"
let kCalendarEventsUrlPath:     String = "feed/?post_type[]=concert&post_type[]=localevents&post_type[]=onlocation"
let kRecentlyPlayedUrlPath:     String = "recently-played/kygofm"
let kLatestItemUrlPath:         String = "feed/"

//MARK: Sort Feeds By:
let kContestSortProperty :      String = "pubDate"
let kCalendarSortProperty :     String = "startDate"
let kLatestSortProperty :       String = "pubDate"

//MARK: DFPAds

let kHighlightBannerAdUnitId:   String = ""
let kNewsBannerAdUnitId:        String = ""
let kShowsBannerAdUnitId:       String = ""//"/1040441/App_Feed_320x100_KTTH" //TODO: might need to see if we have a shows ad DFP
let kShowPageAdUnitId:          String = ""
let kRowAdBannerSize:           CGSize = CGSize(width: 320, height: 50)
let kRowsPerBanner :            Int    = 4
let kMaxBanners :               Int    = -1


//MARK: ScorePage
let kScoresTabLabel:            String = "Latest"
let kScoresURL:                 String = "https://twitter.com/985KYGO"

//MARK: RADIO VC

let kRadioSectionHeader:        String = "EXPLORE"

let kTopLeftBarLogoName:        String = "BlackLogo"
//let kFrontMenuItems :           [MenuItem] = []
//let kFrontMenuItems :           [MenuItem] = [
//    BannerAdMenuItem( position: IndexPath(item: 0, section: 1),
//                      adTag: "/18999272/KYGO_AppBanner_Sponsor"),
//
//    SegueMenuItem(segueIdentifier: "recently_played",
//                  position: IndexPath(item: 1, section: 1),
//                  text: "Recently Played"),
//    TabMenuItem(tabIndex: 1,
//                position: IndexPath(item: 2, section: 1),
//                text: "Contests"),
//    TabMenuItem(tabIndex: 2,
//                position: IndexPath(item: 3, section: 1),
//                text: "Events"),
//    TabMenuItem(tabIndex: 3,
//                position: IndexPath(item: 4, section: 1),
//                text: "Latest From 98.5 KYGO-FM")
//]


//MARK: News View Controller

let kDefaultNewsItemImage = "DefaultNewsIcon"

//MARK: 'More' View Controller

//StationData
let kStationTextNumber :        String = "3037130985"
let kStationPhoneNumber :       String = "telprompt://3037130985"
let kFacebookPage :             String = "98.5KYGO"
let kFacebookPageId :           String = "42754316644"
let kTwitterPage :              String = "985KYGO"
let kStationWebPage :           String = "http://kygo.com"
let kStationAppDownload :       String = "https://itunes.apple.com/us/app/kiro-radio/id573660323?mt=8"
let kStationAppDownload2 :      String = "https://itunes.apple.com/us/app/710-espn-seattle/id599739395?ls=1&mt=8"
let kPlayLegendsURL:            String = "http://player.listenlive.co/53021"
let kContestRulesURL:           String = ""

//StationData Labels
let kTextStationLabel :         String = "Text the station"
let kCallStationLabel :         String = "Call the station"
let kFacebookStationLabel :     String = "Like us on Facebook"
let kTwitterStationLabel :      String = "Follow us on Twitter"
let kWebsiteStationLabel :      String = "Latest at kygo.com"
let kDownloadAppStationLabel :  String = "Download the KOSI FM app"
let kDownloadAppStationLabel2 : String = "Download the 710 ESPN Seattle app"
let kPlayLegendsURLLabel:       String = "Play KYGO Legends Stream"

//let kMoreMenuItems : [MenuItem] = []
//let kMoreMenuItems : [MenuItem] = [
//    LocationServicesToggleMenuItem(position: IndexPath(item: 0, section: 0) ),
//    PushNotificationsToggleMenuItem(position: IndexPath(item: 1, section: 0) ),
//    
//    WebMenuItem(url: kPlayLegendsURL,
//                position: IndexPath(item: 2, section: 0),
//                text: kPlayLegendsURLLabel),
//    CallStationMenuItem(position: IndexPath(item: 3, section: 0),
//                        text : kCallStationLabel,
//                        phoneNumber: kStationPhoneNumber),
//    TextStationMenuItem(text: kTextStationLabel,
//                        position: IndexPath(item: 4, section: 0) ,
//                        stationTextNumber: kStationTextNumber),
//    
//    FacebookStationMenuItem(text : kFacebookStationLabel,
//                            position: IndexPath(item: 5, section: 0),
//                            pageId: kFacebookPageId,
//                            pageString : kFacebookPage ),
//    TwitterStationMenuItem(position: IndexPath(item: 6, section: 0),
//                           text : kTwitterStationLabel,
//                           pageString : kTwitterPage ),
//    WebMenuItem(url: kStationWebPage,
//                position: IndexPath(item: 7, section: 0),
//                text: kWebsiteStationLabel),
//    PrivacyMenuItem(position: IndexPath(item: 8, section: 0),
//                    text: kPrivacyStatement ),
//    WebViewContestRulesMenuItem(position: IndexPath(item: 9, section: 0),
//                                text: kContestRulesLabel )
//]

//MARK: In Studio Video

let kInStudioVideoURL = ""

//MARK: UI Tint Color

let kUITintColor : UIColor = UIColor(red: 0/255, green: 102/255, blue: 153/255, alpha: 1.0)

//MARK: Triton
let kTritonMount = "KYGOFMAAC"

let kWebSocket = "KYGOFM"

let kBundleIdentifier = "com.bonneville.kygo"
//MARK: Google IMA


let kLivePrerollVastTag = "https://pubads.g.doubleclick.net/gampad/ads?sz=400x300&iu=/18999272/KYGO_App_PreRoll&impl=s&gdfp_req=1&env=vp&output=vast&unviewed_position_start=1&url=[referrer_url]&description_url=[description_url]&correlator=[timestamp]"

//Vast tag b/t listens
let kOnDemandPrerollVastTag = "https://pubads.g.doubleclick.net/gampad/ads?sz=400x300&iu=/18999272/KKFN_OnDemand_PreRoll&ciu_szs=4x1,6x1&impl=s&gdfp_req=1&env=vp&output=vast&unviewed_position_start=1&url=[referrer_url]&description_url=[description_url]&correlator=[timestamp]"

let kWatchLivePrerollVastTag = ""

//MARK; Defaults
let kDefaultStatus = "KYGO-FM"


//MARK: Sponsor
let kNavSponsorLink = "http://www.usmortgages.com/"
let kShowPoweredBy = false
let kSponsorName = "US Mortgages"

//MARK: Nielsen
//TODO: Nielsen info for KSFI  needs to be changed
let kNielsenAppId = "P9659AA82-E14B-489B-9443-4FFB601EB15B"
let kNielsenSFCode = "DRM"
let kNielsenStationIdentifier = "KYGO-FM"
let kNielsenProvider = "Bonneville"
let kNielsenPodcastPrefix = "KYGOFMPODCAST"

//MARK: Radio Mode
let kRadioNowPlayingWatchButtonMode = false
let kNowPlayingButtonColor = UIColor.clear
let kNowPlayingButtonFontSize = 12
let kNowPlayingButtonBorderColor = UIColor.white
let kLiveVideoUrl = ""

let kUseConversionTracking = false
let kConversionID = ""
let kConversionLabel = ""
let kConversionValue = ""

//var kDataSources : [DataSourceKey : DataSource] = [
//    .Schedule : ScheduleDataSource(urlRoot: kScheduleUrlRoot, urlPath: kSchedulePath),
//    .Contests : ContestsDataSource(urlRoot: kContestsUrlRoot, urlPath: kContestsUrlPath),
//    .Latest : LatestItemDataSource(urlRoot: kLatestItemUrlRoot, urlPath: kLatestItemUrlPath),
//    .CalendarEvents : CalendarEventsDataSource(urlRoot: kCalendarEventsUrlRoot, urlPath: kCalendarEventsUrlPath),
//    .RecentlyPlayed : RecentlyPlayedDataSource(urlRoot: kRecentlyPlayedUrlRoot, urlPath: kRecentlyPlayedUrlPath),
//    .Blackout : BlackoutDataSource(urlRoot: kBlackoutsUrlRoot, urlPath: kBlackoutsPath),
//]

#if DEBUG
let kLocalyticsId = "11ddbb93d5018e1990c0800-b8ebb406-fa09-11e6-f512-0079ba19c68c"
#else
let kLocalyticsId = "3bd48c3242c8d2950df5f4b-bf7f89b6-f9bd-11e6-9bf7-004b7c354703"
#endif


//MARK: New Stuff
let kContestsBannerAdUnitID = "/18999272/KYGO_App_Highlights"
let kCalendarEventsBannerAdUnitID = "/18999272/KYGO_App_Feed"
let kLatestBannerAdUnitID = "/18999272/KYGO_App_Feed"


//MARK: Soft Ask
let kLocationSoftAskTitle = "Stay in the know"
let kLocationSoftAskDescription = "Get push notifications to see where 98.5 KYGO-FM will be, music, and entertainment news and exclusive contests."

//MARK: UNIVERSAL LINKS
//let kLinkMap : [ActivityTypeEntity : String] = [
//    .ListenLive : "",
//    .ListenToPodcast : "",
//    .ListPodcasts : "",
//    .ListShowAudioPodcasts : "",
//    .WatchLive : ""
//]
//
//let kTabMap : [ActivityTypeEntity : Int] = [
//    .ListenLive : 0,
//    .ListenToPodcast : 1,
//    .ListPodcasts : 1,
//    .ListShowAudioPodcasts : 1,
//    .WatchLive : 0
//]
//
//let kLinkToPodcastShowIdentifier = {(link: URL) -> String? in
//    return link.getQueryStringValueForKey(key: "sid")
//    
//}
//
//let kLinkToPodcastEpisodeIdentifier = {(link: URL) -> String? in
//    return link.getQueryStringValueForKey(key: "a")
//}

let kValidShowId = "2055"
let kValidShowName = "Stuff Your Dad Knows"
let kValidPodcastId = "14866"
